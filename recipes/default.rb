#
# Cookbook Name:: joshexample
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

test = data_bag_item("notmybagbaby","josh")
secret = data_bag_item("secrets","encrypted")

Chef::Log.warn("The contents of the unencrypted data bag are '#{test['secrets']}'")
Chef::Log.warn("The contents of the encrypted data bag are '#{secret['secrets']}'")
