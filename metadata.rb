name 'joshexample'
maintainer 'Anthony Robinson'
maintainer_email 'milrobin@starbucks.com'
license 'all_rights'
description 'Installs/Configures joshexample'
long_description 'Installs/Configures joshexample'
version '0.1.0'

# If you upload to Supermarket you should set this so your cookbook
# gets a `View Issues` link
# issues_url 'https://github.com/<insert_org_here>/joshexample/issues' if respond_to?(:issues_url)

# If you upload to Supermarket you should set this so your cookbook
# gets a `View Source` link
# source_url 'https://github.com/<insert_org_here>/joshexample' if respond_to?(:source_url)
